import { combineReducers } from "redux";

const tokenReducer = (state = null, action) => {
  switch (action.type) {
    case "LOGIN": {
      return action.payload;
    }
    default: {
      return state;
    }
  }
};
const jobsReducer = (state = null, action) => {
  switch (action.type) {
    case "GET_JOBS": {
      return action.payload;
    }
    default: {
      return state;
    }
  }
};

export default combineReducers({
  token: tokenReducer,
  jobs: jobsReducer,
});
