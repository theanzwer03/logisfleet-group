import "date-fns";
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { LogisFleetAPI } from "../apis/Logisfleet";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import JobTable from "./JobTable";

const Dashboard = () => {
  const dispatch = useDispatch();
  const { token, jobs } = useSelector((state) => state);
  const [formValue, setFormValue] = useState({ pageSize: 500 });
  const [selectedFromDate, setSelectedFromDate] = useState("2021-04-01");
  const [selectedToDate, setSelectedToDate] = useState("2021-07-31");
  const handleFormChange = (e) => {
    const { name, value } = e.target;
    setFormValue({
      ...formValue,
      [name]: value,
    });
  };
  const handleSubmit = () => {
    LogisFleetAPI(token)
      .get("/job", {
        params: {
          currentPage: 1,
          pageSize: formValue.pageSize,
          fromDate: selectedFromDate,
          toDate: selectedToDate,
        },
      })
      .then((response) => {
        dispatch({ type: "GET_JOBS", payload: response.data });
        console.log("RESPONSE", response);
      })
      .catch((error) => {
        console.log(error);
        throw new Error(error);
      });
  };
  return (
    <div>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="yyyy/MM/dd"
            margin="normal"
            id="date-picker-inline"
            label="Date from"
            value={selectedFromDate}
            onChange={(date, formattedDate) => setSelectedFromDate(date)}
            KeyboardButtonProps={{
              "aria-label": "change From date",
            }}
          />
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="yyyy/MM/dd"
            margin="normal"
            id="date-picker-inline"
            label="Date To"
            value={selectedToDate}
            onChange={(date, formattedDate) => setSelectedToDate(date)}
            KeyboardButtonProps={{
              "aria-label": "change To date",
            }}
          />
          <FormControl margin="normal">
            <InputLabel id="demo-simple-select-label">Page Size</InputLabel>
            <Select
              labelId="page-size-select-label"
              id="page-size-select"
              name="pageSize"
              value={formValue.pageSize}
              onChange={handleFormChange}
            >
              <MenuItem value={50}>50</MenuItem>
              <MenuItem value={100}>100</MenuItem>
              <MenuItem value={300}>300</MenuItem>
              <MenuItem value={500}>500</MenuItem>
            </Select>
          </FormControl>
          <button onClick={handleSubmit}>Search</button>
        </Grid>
      </MuiPickersUtilsProvider>
      {jobs && <JobTable data={jobs}></JobTable>}
    </div>
  );
};

export default Dashboard;
