import React, { useState } from "react";
import axios from "axios";
import { TextField } from "@material-ui/core";
import { useDispatch } from "react-redux";
const Login = () => {
  const dispatch = useDispatch();
  const [formValue, setFormValue] = useState({
    email: "test@logisfleet.com",
    password: "TestUser321",
  });
  const HandleSubmit = (e) => {
    e.preventDefault();
    axios.post("https://test-api.logisfleet.com/auth/login", formValue).then(
      (response) => {
        dispatch({ type: "LOGIN", payload: response.data });
      },
      (error) => {
        throw new Error(error);
      }
    );
  };
  const HandleChange = (e) => {
    const { name, value } = e.target;
    setFormValue({
      ...formValue,
      [name]: value,
    });
    console.log(formValue);
  };

  return (
    <form noValidate autoComplete="off" onSubmit={(e) => HandleSubmit(e)}>
      <TextField
        name="email"
        label="Email"
        value={formValue.email}
        onChange={(e) => HandleChange(e)}
        required
      />
      <TextField
        name="password"
        label="Password"
        value={formValue.password}
        onChange={(e) => HandleChange(e)}
        type="password"
        required
      />
      <button variant="contained">Submit</button>
    </form>
  );
};

export default Login;
