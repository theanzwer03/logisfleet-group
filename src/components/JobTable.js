import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@material-ui/core";
const JobTable = ({ data }) => {
  const keysToShow = [
    "jobNumber",
    "customerName",
    "billingName",
    "jobDate",
    "jobTimeSpecific",
    "jobTimeFrom",
    "jobTimeTo",
    "actualJobTimeSpecific",
    "inProgressTime",
    "jobAttemptCompletedDate",
    "jobTemplateName",
    "driverName",
    "vehicleName",
  ];
  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            {keysToShow.map((key) => {
              return <TableCell>{key}</TableCell>;
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.data.map((row) => {
            return (
              <TableRow>
                {keysToShow.map((key) => {
                  return <TableCell>{row[key]}</TableCell>;
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default JobTable;
