import axios from "axios";
const url = "https://test-api.logisfleet.com";
export const LogisFleetAPI = (token) => {
  return axios.create({
    baseURL: url,
    headers: {
      Authorization: `${token}`,
    },
  });
};
