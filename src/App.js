import React from "react";
import { Box } from "@material-ui/core";
import Login from "./components/Login";
import Dashboard from "./components/Dashboard";
import { useSelector } from "react-redux";
function App() {
  const token = useSelector((state) => state.token);
  return <Box>{token ? <Dashboard /> : <Login />}</Box>;
}

export default App;
